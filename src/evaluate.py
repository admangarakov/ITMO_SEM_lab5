import json
import pandas as pd
from sklearn.metrics import confusion_matrix, f1_score
import joblib
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, ConfusionMatrixDisplay

if __name__ == '__main__':

    X_test = pd.read_csv('data/prepared/X_test.csv')
    y_test = pd.read_csv('data/prepared/y_test.csv')

    clf = joblib.load('model/model.joblib')
    prediction = clf.predict(X_test)

    # check accuracy
    accuracy = accuracy_score(y_test, prediction)
    print(accuracy)

    json.dump(
        obj={
            'accuracy_score': accuracy
        },
        fp=open('metrics/accuracy.json', 'w')
    )
    # Plot it
    # disp = ConfusionMatrixDisplay.from_estimator(
    #     clf, X_test, y_test, normalize="true", cmap=plt.cm.Blues
    # )
    # plt.savefig("plot.png")

